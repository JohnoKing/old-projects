/*
 * Copyright © 2018-2020 Johnothan King. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
 * fork - daemonizes any given command
 * This command is not dependent on LeanInit (it can be used separately).
 */

#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	// Usage
	if(argc == 1) {
		printf("You must provide fork with arguments!\n");
		printf("Usage: fork 'enclosed command' ...\n");
		return 1;
	}

	// Detach into a separate process
	pid_t daemon = fork();
	if(daemon == 0) {
		setsid();
		return execl("/bin/sh", "/bin/sh", "-c", argv[1], (char*)0);
	}

	// Return the process id
	return daemon;
}
